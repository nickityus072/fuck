---
title: "MEGA – Mobile Electro Guitar Academy"
date: 2019-06-26T15:52:30+02:00
draft: false
---
[mega.jpg]: ../mega.jpg "Mobile Elektro Guitar Academy"

Ich verstehe meinen Gitarrenunterricht als erweiterten Musikunterricht mit Wissen über Elektronik. 
Die Harmonielehre, der Rhythmus, das Notenblattlesen, die Improvisation und das 
experimentelle Musizieren gehören selbstverständlich zu dem individuellen Unterricht mit dazu.
An praktischen Beispielen lernst du neben der E-Gitarre auch die Grundlagen der elektronischen Musik kennen. 

Und das Beste: Das alles findet bequem bei dir zuhause statt!

Der Unterricht in Luzern, Zug und Zürich kostet CHF 150.- pro volle Stunde 
(andere Städte auf Anfrage). Interessiert? Dann schreibe mir eine [E-Mail](mailto:felix.baenteli@protonmail.com).

<br>

**Persönliche Instrumentenvermietung** 

Hast du noch kein Instrument und / oder willst du verschiedene Mikrofone, Verstärker und Effektgeräte 
ausprobieren? Ob für Anlässe das richtige PA oder für den Unterricht das passende Instrument. 
Kein Problem! Ich vermiete Gitarren, Verstärker und verschiedene Loop- und Effektgeräte.

[![mega][mega.jpg]](../vermietung)

**Reparaturen**

Kabel mit Wackelkontakten und E-Gitarren kann man wie viele andere Sachen reparieren. 
Im Verein [LABOR Luzern](https://www.laborluzern.ch) in der offenen Werkstatt in Kriens ist jeden 
Abend ab 20:00Uhr geöffnet. Ich bin da auch Mitglied und kann gegen eine Spende gerne helfen.
