---
title: "Computer- und Softwarekurse"
date: 2019-06-26T15:52:23+02:00
draft: false
---
[kilo.jpg]: ../kilo.jpg "Key In Living Opensource"

Ich gebe mein Grundwissen über verschiedene Open Source Programme weiter. 
Dieses Angebot sind Einsteiger-Software-Kurse, die vor allem auf Kulturschaffende 
und solche, die es noch werden wollen, ausgerichtet sind. 

- Einfache Funktionen in [**LibreOffice Calc**](../kurs1/) 6.3.1

- Basic 3D modellieren mit [**Blender**](../kurs2/) 2.8 

- Musikmachen mit Livecoding in [**SonicPi**](../kurs3/) 3.1.0 

<br>

Die Kurse gehen jeweils 2h. Ein Kurs kostet CHF 300.- geteilt durch die 
Anzahl Teilnehmenden und ab 5 Anmeldungen CHF 60.- pro Person. 
Die [Anmeldung](mailto:felix.baenteli@protonmail.com) geschieht mit einer E-Mail an mich.

<br>

**Weitere Kursangebote**

Mit der Workshopagentur [Actioncy GmbH](https://www.actioncy.ch) arbeite ich an vielen weiteren mechatronischen 
Kunst-Workshops rund um den Themenkreis von freier Soft- und Hardware.
Für Gross und Klein bieten wir in der ganzen Schweiz Workshops und Teamevents zu Sound, Visualz und Robotik an. 

[Kontaktieren](mailto:felix.baenteli@actioncy.ch)  Sie uns!

<br>

**DIY Workshops**

Für verschiedene mechatronische Kunst-Lötworkshops habe ich elektronische Leiterplatinen / PCBs gezeichnet. 
Die elektronischen Schaltungen sind aus der Open Source Community und die Entwicklung ist auf dem Wiki der 
Schweizerischen Gesellschaft für Mechatronische Kunst – [SGMK](https://wiki.sgmk-ssam.ch/wiki/Main_Page) dokumentiert.

[![kilo.jpg][kilo.jpg]](../pcbs)

Die Plattform [Kitspace](https://www.kitspace.org) macht möglich, dass man für Workshops einfach die Menge Bausätze 
bestellen kann, die man für den Lötunterricht braucht. Ich arbeite daran meine Designs da auch raufzuladen.

<br>
