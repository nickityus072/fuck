---
title: "Bilder & Objekte"
date: 2019-06-26T15:52:42+02:00
draft: false
---
[giga.jpg]: ../giga.jpg "GIGA"

Meine gestalterisch Tätigkeit reicht von rauhem Grafikdesign, crazy Flyer, irren 
Logos bis zu experimentellen 3D-Renderings, schrägen Objekten und wilden Videos. 

[![giga.jpg][giga.jpg]](../designkunst)

Ich bin ein leidenschaftlicher Zeichner! Es gibt hier in dieser [Galerie](../designkunst) einige meiner persönlichen Favoriten... 
Alle Zeichnungen und Bilder sind im angesagtem Instragram Square im Format 21 x 21cm als Spezialdruck auf Kartoffelpapier erhältlich für CHF 145.-.

<br>

**Film- und Videoprojekte**

Video- und Filmprojekte gehören seit dem Beginn meiner gestalterischen kreativen Arbeit wie eine Selbstverständlichkeit 
mit dazu. In meinen Videoprojekten auf [Vimeo](https://vimeo.com/user71901967) findet ihr eine Ansammlung von meinen bewegten 
und bewegenden Bildexperimenten.

<a href="https://vimeo.com/295773310" target="_blank"><img src="" 
alt="Video von Vj Bentley FX" width="240" height="180" border="10" /></a>

Ob Auftragsarbeit, Kunstwerk oder Freizeitangebot für Jugendliche; ich bin offen für Anfragen zu verschiedensten Film-, Video- und Visualzprojekte.   

<br>

**Geräte, Instrumente und andere gehackte Hardware**

Ausrangierte elektronische Geräte und Musikinstrumente kann man so modifizieren und anpassen, dass sie 
wieder Spass machen! Man nennt dies Circuit Bendig. Hier eine [Galerie](../designkunst1) von meinen unverkauflichen DIY-Instrumente!

[![giga.jpg][giga.jpg]](../designkunst)

<br>
