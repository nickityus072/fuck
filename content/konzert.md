---
title: "Soundperformances"
date: 2019-06-26T15:53:06+02:00
draft: false
---
[mega.jpg]: ../mega.jpg "Konzert bei Urs Gaudenz"

In verschiedenen musikalischen Konstellationen organisiere ich Jamsessions und mache Soundperformances und Konzerte. 

<br>

**Die nächsten Auftritte**

Am 11. November bin ich in der Boulderhalle in Winterthur als Host und MC!

<br>

**Vergangene Auftritte**

Soundperformance und Experimentierlabor am 23.8.19 im Schaufenster vom LOEB in Bern mit Mysterious F und kaascat
![alt text][mega.jpg]

Soundperformance und Livesendung mit Mysterious F, kaascat und DJ Murat an den DJ Sessions am 1. August 2019 im Radio LoRa in Zürich
![alt text][mega.jpg]

Soundperformance an der Kinderdisco im N49 mit Mysterious F und kaascat am 22. Juli 2019 
![alt text][mega.jpg]

Soundperformance mit Mysterious F am Teiggifest am 22. September 2018 
![alt text][mega.jpg]

Soundperformance mit Mysterious F für Urs Gaudenz am 18. Juni 2018 im B74 Raum für Kunst in Luzern
 ![alt text][mega.jpg]

<br>

**Radio LoRa**
Im Radio LoRa kann man mich seit 13 Jahren jeden 4. Samstag im Monat als MC hören.
