---
title: "Kunstprojektunterstützung"
date: 2019-06-26T15:52:42+02:00
draft: false
---
[giga.jpg]: ../giga.jpg "GIGA"

Plant ihr ein komplexes Design- oder Kunstprojekt und wisst bei technischen oder künstlerischen Fragen nicht weiter? 
Ich biete Projektbetreuung von interaktiven Installationen jeglicher Art an. 
Gerne berate ich euch über mechatronische Lösungen und helfe je nach Abmachung auch bei der Ausführung und Inszenierung.

![giga][giga.jpg]

Eine viertelstündige erste Beratung ist gratis, danach kostet es ohne andere Abmachung CHF 150.- pauschal und jede weitere Stunde CHF 50.-.
Schreibt mir am besten für das erste Gespräch eine [E-Mail](mailto:felix.baenteli@protonmail.com) mit kurzer Beschreibung eures Projektes. 

<br>




