---
title: "LibreOffice Calc 6.3.1"
date: 2019-06-26T15:52:23+02:00
draft: false
---
[kilo.jpg]: ../kilo.jpg "Key In Living Opensource"

Einfache Funktionen LibreOffice Calc 6.3.1

![kilo.jpg][kilo.jpg]

Die Kurse gehen jeweils 2h. Ein Kurs kostet CHF 300.- geteilt durch die 
Anzahl Teilnehmenden und ab 5 Anmeldungen CHF 60.- pro Person. 
Die [Anmeldung](mailto:felix.baenteli@protonmail.com) geschieht mit einer E-Mail an mich.

<br>
